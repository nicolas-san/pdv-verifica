﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace Verifica_PDV
{
    public class VerificaPDV
    {
        static void Main(string[] args)
        {
            string matricula;
            WriteLine("Digite a matrícula: ");
            matricula = ReadLine();

            

            /*============CONVERSÕES===========*/
            char p1 = matricula[0];
            int num1 = int.Parse(p1.ToString());

            char p2 = matricula[1];
            int num2 = int.Parse(p2.ToString());

            char p3 = matricula[2];
            int num3 = int.Parse(p3.ToString());

            char p4 = matricula[3];
            int num4 = int.Parse(p4.ToString());

            char p5 = matricula[4];
            int num5 = int.Parse(p5.ToString());
            /*================FIM DAS CONVERSÕES=================*/
            
            /* =========================RESULTADO FINAL======================== */
            
            WriteLine("Senha PDV: " + matricula + Frm8(num1, num2, num3, num4, num5) + 
                Frm17(num1, num2, num3, num4, num5));

            /* =========================RESULTADO FINAL======================== */

            /*=================ÁREA DE TESTES================*/
            //Para conseguir testar cada fórmula, é necessário descomentar o comando WriteLine();
            //de cada método

            //Frm1(num1);
            //Frm2(num2);
            //Frm3(num3);
            //Frm4(num4);
            //Frm5(num5);
            //Frm6(num1, num2, num3, num4, num5);
            //Frm7(num1, num2, num3, num4, num5);
            //Frm8(num1, num2, num3, num4, num5);
            //Frm9(num1);
            //Frm10(num2);
            //Frm11(num3);
            //Frm12(num4);
            //Frm13(num5);
            //Frm14(num1, num2, num3, num4, num5);
            //Frm15(num1, num2, num3, num4, num5);
            //Frm16(num1, num2, num3, num4, num5);
            //Frm17(num1,num2,num3,num4,num5);    
            /*=================FIM DA ÁREA DE TESTES================*/
            Read();
        }

        static int Frm1(int num1)
        {            
            int res1 = num1 * 3;
            //WriteLine("F1: "+res1);
            return res1;
        }

        static int Frm2(int num2)
        {
            int res2 = num2;
            //WriteLine("F2: "+res2);
            return res2;
        }

        static int Frm3(int num3)
        {
            int res3 = num3 * 3;
            //WriteLine("F3: "+res3);
            return res3;
        }

        static int Frm4(int num4)
        {
            int res4 = num4;
            //WriteLine("F4: " + res4);
            return res4;
        }

        static int Frm5(int num5)
        {
            int res5 = num5 * 3;
            //WriteLine("F5: " + res5);
            return res5;
        }

        static int Frm6(int num1, int num2, int num3, int num4, int num5)
        {
            int conta = ((Frm1(num1) + Frm2(num2) + Frm3(num3) + Frm4(num4) + Frm5(num5)) + 9) / 10;
            int res6 = conta * 10;
            // WriteLine("F6: " + r6);
            return res6;
            //(inteiro(({= Frm_1} + {= Frm_2}+ {= Frm_3} + {= Frm_4} + {= Frm_5}+9) / 10)) *10 = 40
            //((18 + 6 + 3 + 0 + 6) + 9) / 10 = 4,2---- inteiro = 4 * 10 = 40
        }

        static int Frm7(int num1, int num2, int num3, int num4, int num5)
        {
            int res7 = (Frm1(num1) + Frm2(num2) + Frm3(num3) + Frm4(num4) + Frm5(num5));
            //({=Frm_1} + {=Frm_2} + {=Frm_3} + {=Frm_4} + {=Frm_5}) = 33
            //WriteLine(res7);
            return res7;
        }

        /* APURAÇÃO PRIMEIRO NÚMERO - FÓRMULA 8*/
        static int Frm8(int num1, int num2, int num3, int num4, int num5)
        {
            int res8 = (Frm6(num1, num2, num3, num4, num5) - Frm7(num1, num2, num3, num4, num5));
            //WriteLine(res8);    
            return res8;
        }
        
        static int Frm9(int num1)
        {
            int res9 = num1;
            //WriteLine(res9);
            return res9;
        }
        static int Frm10(int num2)
        {
            int res10 = num2*3;
            //WriteLine(res10);
            return res10;
        }

        static int Frm11(int num3)
        {
            int res11 = num3;
            //WriteLine(res11);
            return res11;   
        }

        static int Frm12(int num4)
        {
            int res12 = num4 * 3;
            //WriteLine(res12);   
            return res12;
        }

        static int Frm13(int num5)
        {
            int res13 = num5;
            //WriteLine(res13);   
            return res13;   
        }

        static int Frm14(int num1, int num2, int num3, int num4, int num5)
        {
            int res14 = (Frm8(num1, num2, num3, num4, num5) * 3);
            //WriteLine(res14);   
            return res14;
        }

        static int Frm15(int num1, int num2, int num3, int num4, int num5)
        {
            int res15 = ((Frm9(num1) + Frm10(num2) + Frm11(num3) + Frm12(num4) + Frm13(num5) + Frm14(num1, num2, num3, num4, num5) + 9) / 10) * 10;
            //WriteLine(res15);
            return res15;
            //(inteiro(({=Frm_9} + {=Frm_10} + {=Frm_11} + {=Frm_12} + {=Frm_13} + {=Frm_14} +9) / 10)) * 10
            //((((6 + 18 + 1 + 0 + 2 + 21) + 9) / 10(inteiro)) * 10) = 57 / 10 = 5,7 - inteiro = 5 * 10 = 50
        }

        static int Frm16(int num1, int num2, int num3, int num4, int num5)
        {
            int res16 = (Frm9(num1) + Frm10(num2) + Frm11(num3) + Frm12(num4) + Frm13(num5) + Frm14(num1, num2, num3, num4, num5));
            //WriteLine(res16);
            return res16;
            //({= Frm_9} + {= Frm_10} + {= Frm_11} + {= Frm_12} + {= Frm_13} + {= Frm_14}) = 48
            //(6 + 18 + 1 + 0 + 2 + 21)
        }

        /* APURAÇÃO SEGUNDO NÚMERO - FÓRMULA 17 */
        static int Frm17(int num1, int num2, int num3, int num4, int num5)
        {
            int res17 = (Frm15(num1, num2, num3, num4, num5) - Frm16(num1, num2, num3, num4, num5));
            //WriteLine(res17);
            return res17;
        }
    }
}
